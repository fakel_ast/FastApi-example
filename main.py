import time
from typing import Annotated

from fastapi import FastAPI, BackgroundTasks, Depends, Header, HTTPException, APIRouter
from pydantic import BaseModel, Field, conint

app = FastAPI()

api_router = APIRouter(prefix='/api/v1', tags=['Апи роуты...'])


class Item(BaseModel):
    id: int
    q: str | None


class ItemWithDescription(BaseModel):
    item_id: int = Field(description='Описание поля...')
    q: str | None


class ItemQuery(BaseModel):
    limit: conint(ge=10) = 10
    offset: int | None = 0


async def background_print():
    print('Sleep start')
    time.sleep(3)
    print('Sleep end')


async def verify_token(x_token: Annotated[str, Header()]):
    if x_token != "fake-super-secret-token":
        raise HTTPException(status_code=400, detail="X-Token header invalid")


@app.get("/items/{item_id}", response_model=Item)
async def read_item(item_id: int, q: str | None = None):
    return {"item_id": item_id, "q": q}


@api_router.post("/items1/", response_model=Item)
async def read_item_1(item: Item):
    return {"id": item.id, "q": item.q}


@api_router.post("/items2/", response_model=Item)
async def read_item_2(item: Item, background_tasks: BackgroundTasks):
    background_tasks.add_task(background_print)
    return {"id": item.id, "q": item.q}


@api_router.get("/items-query/", response_model=ItemQuery)
async def item_query(query: ItemQuery = Depends()):
    return query


@api_router.get("/items-with-token/", dependencies=[Depends(verify_token)])
async def read_items_with_token():
    return [{"item": "Foo"}, {"item": "Bar"}]


app.include_router(api_router)
